public class Refrigerator
{
	public int warranty;
	public String brand;
	public int lowestTemp;	
	
	public void refrigerate()
	{
		System.out.println("Please give your refrigerator 24 hours to reach a stable temperature upon initial startup");
	}
	
	public void freeze()
	{
		if (lowestTemp <= 0)
		{
			System.out.println("Your food product will freeze in approximately 2 hours");
		}
		else
		{
			System.out.println("Please lower your freezer's temperature to 0 degree or lower for optimal effectiveness");
		}
	}
}
