import java.util.Scanner;

public class ApplianceStore
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Refrigerator[] appliances = new Refrigerator[4];
		
		for (int i = 0; i < appliances.length; i++)
		{
			appliances[i] = new Refrigerator();			
			
			System.out.println("Please enter the brand of your refrigerator: ");
			appliances[i].brand = scan.next();
			
			System.out.println("Please enter the number of year your warranty will last: ");
			appliances[i].warranty = scan.nextInt();
			
			System.out.println("Please enter the lowest temperature(Celsius) your refrigerator can achieve: ");
			appliances[i].lowestTemp = scan.nextInt();
		}
		
		 System.out.println("Brand: " + appliances[3].brand);
		 System.out.println("Warranty duration: " + appliances[3].warranty);
		 System.out.println("Lowest temperature: " + appliances[3].lowestTemp);
		 
		 appliances[0].refrigerate();
		 appliances[0].freeze();
	}
}